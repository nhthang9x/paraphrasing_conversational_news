import pandas as pd
import re
import numpy as np
from nltk import word_tokenize, sent_tokenize
import random
import csv
import os
import shutil
import scipy

random.seed('345')


class Main():
    def __init__(self):
        super().__init__()
        list_1 = self.extract_number_and_sentence(column_idx=1)
        list_2 = self.extract_number_and_sentence(column_idx=2)

        self.glove_list = []
        self.model = self.loadGloveModel(gloveFile="glove.6B.50d.txt")

        if os.path.isdir('output'):
            shutil.rmtree('output')
        os.makedirs('output')

        self.create_csv(path="Same_number_pairs")
        self.pair_of_same_number(input_1=list_1, input_2=list_2, output_path="Same_number_pairs")

    def extract_number_and_sentence(self, column_idx=1):
        df = pd.read_csv(filepath_or_buffer="result.csv", header=0).values
        list_of_dict = []
        for idx, doc_1, doc_2, doc_3, doc_4, doc_5 in df:
            dictionary = {}
            if column_idx == 1:
                column = doc_1
            elif column_idx == 2:
                column = doc_2
            elif column_idx == 3:
                column = doc_3
            elif column_idx == 4:
                column = doc_4
            elif column_idx == 5:
                column = doc_5
            else:
                print("error")

            for sent in sent_tokenize(text=str(column)):
                numbers_in_sent = []
                for word in word_tokenize(text=sent):
                    re_edx = re.search("[0-9]", word)
                    if re_edx:
                        numbers = re.findall(r'\d+', word)
                        numbers_in_sent.append(numbers[0])
                if len(numbers_in_sent) > 0:
                    dictionary[str(numbers_in_sent)] = str(sent)
            list_of_dict.append(dictionary)

        return list_of_dict

    def create_csv(self, path):
        """
        Create a .csv file with 6 column: the first column is orderring, the second column for the text number 1, the third column for the text number 2
        :param path: The path to output .csv file
        :return:
        """
        with open("output/{}.csv".format(path), mode="a", encoding="utf-8") as csvfile:
            field = ["Order", "Text_1", "Text_2", "Cosine_Similarity"]
            csv_file = csv.DictWriter(f=csvfile, fieldnames=field)
            csv_file.writeheader()
        csvfile.close()

    def pair_of_same_number(self, input_1, input_2, output_path):
        count = 0
        for i in range(len(input_1) - 1):
            for key in (input_1[i].keys() & input_2[i].keys()):
                if str(input_1[i][key] != str(input_2[i][key])):
                    count = count + 1
                    with open("output/{}.csv".format(output_path), mode="a", encoding="utf-8") as csv_file:
                        writer = csv.writer(csv_file)
                        writer.writerow([count, str(input_1[i][key]), str(input_2[i][key]), str(
                            self.cosine_distance_wordembedding_method(s1=str(input_1[i][key]).lower(),
                                                                      s2=str(input_2[i][key]).lower()))])
                    csv_file.close()

    def loadGloveModel(self, gloveFile):
        print("Loading Glove Model")
        pretrained_file = open(gloveFile, 'r')
        model = {}
        for line in pretrained_file:
            splitted_line = line.split()
            word = splitted_line[0]
            self.glove_list.append(str(word).lower())
            embedding = np.array([float(val) for val in splitted_line[1:]])
            model[word] = embedding
        print("Done.", len(model), " words loaded!")
        return model

    def cosine_distance_wordembedding_method(self, s1, s2):
        vector_1 = np.mean(
            [self.model[word] if word in self.glove_list else [0] * 50 for word in word_tokenize(s1.lower())], axis=0)
        vector_2 = np.mean(
            [self.model[word] if word in self.glove_list else [0] * 50 for word in word_tokenize(s2.lower())], axis=0)
        cosine = scipy.spatial.distance.cosine(vector_1, vector_2)
        result = round((1 - cosine) * 100, 2)
        print('Word Embedding method with a cosine distance asses that our two sentences are similar to',
              result, '%')

        return result


if __name__ == '__main__':
    Main()
