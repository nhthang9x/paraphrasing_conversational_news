import pandas as pd
import numpy as np
from nltk import word_tokenize, sent_tokenize
import re
from nltk.corpus import words
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
import csv
import scipy
import os
import shutil


class Main():
    def __init__(self):
        super().__init__()

        if os.path.isdir('output'):
            shutil.rmtree('output')
        os.makedirs('output')

        self.df = pd.read_csv(filepath_or_buffer="result.csv", header=0, nrows=70).values
        self.ps = PorterStemmer()
        self.stop_words_1 = list(pd.read_fwf(filepath_or_buffer="stopwords.txt").values)
        self.stop_words_2 = set(stopwords.words('english'))
        self.glove_list = []
        self.model = self.loadGloveModel(gloveFile="glove.6B.50d.txt")

        list_1 = self.extract_column(column_idx=1)
        list_2 = self.extract_column(column_idx=2)

        list_private_name = self.list_of_private_name(list_1, list_2)
        list_document = self.extract_paraphrase_pairs(1, 2, list_private_name)

        self.create_csv(path="Same_privatename_pairs")
        self.write_to_csv(input_text=list_document, output_path="Same_privatename_pairs")

    def extract_column(self, column_idx):
        list_of_dict = []
        for idx, doc_1, doc_2, doc_3, doc_4, doc_5 in self.df:
            if column_idx == 1:
                column = doc_1
            elif column_idx == 2:
                column = doc_2
            elif column_idx == 3:
                column = doc_3
            elif column_idx == 4:
                column = doc_4
            elif column_idx == 5:
                column = doc_5
            else:
                print("error")

            private_words = {}
            if len(str(column)) > 10:
                for word in sent_tokenize(text=column):
                    re_edx = re.search("([A-Z]+[a-z]*)(\s[A-Z]+[a-z]*)*", word)
                    if re_edx and self.ps.stem(str(re_edx[0]).lower()) not in words.words() and str(
                            re_edx[0]).lower() not in self.stop_words_2 and str(
                        re_edx[0]).lower() not in self.stop_words_1:
                        if str(re_edx[0]) not in private_words.keys():
                            private_words['{}'.format(re_edx[0])] = 1
                        else:
                            private_words['{}'.format(re_edx[0])] += 1
            else:
                pass
            list_of_dict.append(private_words)

        return list_of_dict

    def create_csv(self, path):
        """
        Create a .csv file with 6 column: the first column is orderring, the second column for the text number 1, the third column for the text number 2
        :param path: The path to output .csv file
        :return:
        """
        with open("output/{}.csv".format(path), mode="a", encoding="utf-8") as csvfile:
            field = ["Order", "Text_1", "Text_2", "Cosine_Similarity"]
            csv_file = csv.DictWriter(f=csvfile, fieldnames=field)
            csv_file.writeheader()
        csvfile.close()

    def list_of_private_name(self, list_1, list_2):
        list_private_name = []
        for i in range(len(list_1)):
            private_name = []
            if len(list_1[i]) > 0 and len(list_2[i]) > 0:
                for key in (list_1[i].keys() & list_2[i].keys()):
                    if list_1[i][key] == 1 and list_2[i][key] == 1:
                        private_name.append(key)
            else:
                pass
            list_private_name.append(private_name)
        return list_private_name

    def extract_paraphrase_pairs(self, column_idx_1, column_idx_2, list_private_name):
        list_document = []
        for i in range(len(list_private_name)):
            for word in list_private_name[i]:
                document = []
                for sent in sent_tokenize(text=self.df[i][column_idx_1]):
                    if str(word) in str(sent):
                        document.append(str(sent))
                        break
                for sent in sent_tokenize(text=self.df[i][column_idx_2]):
                    if str(word) in str(sent):
                        document.append(str(sent))
                        break
                list_document.append(document)
        return list_document

    def write_to_csv(self, input_text, output_path):
        for idx, row in enumerate(input_text):
            with open("output/{}.csv".format(output_path), mode="a", encoding="utf-8") as csv_file:
                writer = csv.writer(csv_file)
                writer.writerow([idx, str(row[0]), str(row[1]), str(
                    self.cosine_distance_wordembedding_method(s1=str(row[0]).lower(), s2=str(row[1]).lower()))])
            csv_file.close()

    def loadGloveModel(self, gloveFile):
        print("Loading Glove Model")
        pretrained_file = open(gloveFile, 'r')
        model = {}
        for line in pretrained_file:
            splitted_line = line.split()
            word = splitted_line[0]
            self.glove_list.append(str(word).lower())
            embedding = np.array([float(val) for val in splitted_line[1:]])
            model[word] = embedding
        print("Done.", len(model), " words loaded!")
        return model

    def cosine_distance_wordembedding_method(self, s1, s2):
        vector_1 = np.mean(
            [self.model[word] if word in self.glove_list else [0] * 50 for word in word_tokenize(s1.lower())], axis=0)
        vector_2 = np.mean(
            [self.model[word] if word in self.glove_list else [0] * 50 for word in word_tokenize(s2.lower())], axis=0)
        cosine = scipy.spatial.distance.cosine(vector_1, vector_2)
        result = round((1 - cosine) * 100, 2)
        print('Word Embedding method with a cosine distance asses that our two sentences are similar to',
              result, '%')

        return result


if __name__ == '__main__':
    Main()
